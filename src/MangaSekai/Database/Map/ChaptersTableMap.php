<?php

namespace MangaSekai\Database\Map;

use MangaSekai\Database\Chapters;
use MangaSekai\Database\ChaptersQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'chapters' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class ChaptersTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = 'MangaSekai.Database.Map.ChaptersTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'chapters';

    /**
     * The PHP name of this class (PascalCase)
     */
    public const TABLE_PHP_NAME = 'Chapters';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\MangaSekai\\Database\\Chapters';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'MangaSekai.Database.Chapters';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 4;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 4;

    /**
     * the column name for the id field
     */
    public const COL_ID = 'chapters.id';

    /**
     * the column name for the id_series field
     */
    public const COL_ID_SERIES = 'chapters.id_series';

    /**
     * the column name for the pages_count field
     */
    public const COL_PAGES_COUNT = 'chapters.pages_count';

    /**
     * the column name for the number field
     */
    public const COL_NUMBER = 'chapters.number';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Id', 'IdSeries', 'PagesCount', 'Number', ],
        self::TYPE_CAMELNAME     => ['id', 'idSeries', 'pagesCount', 'number', ],
        self::TYPE_COLNAME       => [ChaptersTableMap::COL_ID, ChaptersTableMap::COL_ID_SERIES, ChaptersTableMap::COL_PAGES_COUNT, ChaptersTableMap::COL_NUMBER, ],
        self::TYPE_FIELDNAME     => ['id', 'id_series', 'pages_count', 'number', ],
        self::TYPE_NUM           => [0, 1, 2, 3, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Id' => 0, 'IdSeries' => 1, 'PagesCount' => 2, 'Number' => 3, ],
        self::TYPE_CAMELNAME     => ['id' => 0, 'idSeries' => 1, 'pagesCount' => 2, 'number' => 3, ],
        self::TYPE_COLNAME       => [ChaptersTableMap::COL_ID => 0, ChaptersTableMap::COL_ID_SERIES => 1, ChaptersTableMap::COL_PAGES_COUNT => 2, ChaptersTableMap::COL_NUMBER => 3, ],
        self::TYPE_FIELDNAME     => ['id' => 0, 'id_series' => 1, 'pages_count' => 2, 'number' => 3, ],
        self::TYPE_NUM           => [0, 1, 2, 3, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Id' => 'ID',
        'Chapters.Id' => 'ID',
        'id' => 'ID',
        'chapters.id' => 'ID',
        'ChaptersTableMap::COL_ID' => 'ID',
        'COL_ID' => 'ID',
        'IdSeries' => 'ID_SERIES',
        'Chapters.IdSeries' => 'ID_SERIES',
        'idSeries' => 'ID_SERIES',
        'chapters.idSeries' => 'ID_SERIES',
        'ChaptersTableMap::COL_ID_SERIES' => 'ID_SERIES',
        'COL_ID_SERIES' => 'ID_SERIES',
        'id_series' => 'ID_SERIES',
        'chapters.id_series' => 'ID_SERIES',
        'PagesCount' => 'PAGES_COUNT',
        'Chapters.PagesCount' => 'PAGES_COUNT',
        'pagesCount' => 'PAGES_COUNT',
        'chapters.pagesCount' => 'PAGES_COUNT',
        'ChaptersTableMap::COL_PAGES_COUNT' => 'PAGES_COUNT',
        'COL_PAGES_COUNT' => 'PAGES_COUNT',
        'pages_count' => 'PAGES_COUNT',
        'chapters.pages_count' => 'PAGES_COUNT',
        'Number' => 'NUMBER',
        'Chapters.Number' => 'NUMBER',
        'number' => 'NUMBER',
        'chapters.number' => 'NUMBER',
        'ChaptersTableMap::COL_NUMBER' => 'NUMBER',
        'COL_NUMBER' => 'NUMBER',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('chapters');
        $this->setPhpName('Chapters');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\MangaSekai\\Database\\Chapters');
        $this->setPackage('MangaSekai.Database');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addForeignKey('id_series', 'IdSeries', 'INTEGER', 'series', 'id', true, 10, null);
        $this->addColumn('pages_count', 'PagesCount', 'INTEGER', true, null, null);
        $this->addColumn('number', 'Number', 'REAL', true, null, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('Series', '\\MangaSekai\\Database\\Series', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_series',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ChapterTracker', '\\MangaSekai\\Database\\ChapterTracker', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_chapter',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', 'ChapterTrackers', false);
    }

    /**
     * Method to invalidate the instance pool of all tables related to chapters     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool(): void
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ChapterTrackerTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? ChaptersTableMap::CLASS_DEFAULT : ChaptersTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (Chapters object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = ChaptersTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ChaptersTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ChaptersTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ChaptersTableMap::OM_CLASS;
            /** @var Chapters $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ChaptersTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ChaptersTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ChaptersTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Chapters $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ChaptersTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ChaptersTableMap::COL_ID);
            $criteria->addSelectColumn(ChaptersTableMap::COL_ID_SERIES);
            $criteria->addSelectColumn(ChaptersTableMap::COL_PAGES_COUNT);
            $criteria->addSelectColumn(ChaptersTableMap::COL_NUMBER);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.id_series');
            $criteria->addSelectColumn($alias . '.pages_count');
            $criteria->addSelectColumn($alias . '.number');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(ChaptersTableMap::COL_ID);
            $criteria->removeSelectColumn(ChaptersTableMap::COL_ID_SERIES);
            $criteria->removeSelectColumn(ChaptersTableMap::COL_PAGES_COUNT);
            $criteria->removeSelectColumn(ChaptersTableMap::COL_NUMBER);
        } else {
            $criteria->removeSelectColumn($alias . '.id');
            $criteria->removeSelectColumn($alias . '.id_series');
            $criteria->removeSelectColumn($alias . '.pages_count');
            $criteria->removeSelectColumn($alias . '.number');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(ChaptersTableMap::DATABASE_NAME)->getTable(ChaptersTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a Chapters or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or Chapters object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChaptersTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \MangaSekai\Database\Chapters) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ChaptersTableMap::DATABASE_NAME);
            $criteria->add(ChaptersTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ChaptersQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ChaptersTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ChaptersTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the chapters table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return ChaptersQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Chapters or Criteria object.
     *
     * @param mixed $criteria Criteria or Chapters object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ChaptersTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Chapters object
        }

        if ($criteria->containsKey(ChaptersTableMap::COL_ID) && $criteria->keyContainsValue(ChaptersTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ChaptersTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ChaptersQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
