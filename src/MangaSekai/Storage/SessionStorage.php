<?php declare(strict_types=1);
    namespace MangaSekai\Storage;
    
    use Firebase\JWT\JWT;
    use Firebase\JWT\Key;

    class SessionStorage
    {
        /** @var string The token this session storage belongs to */
        private string $token = '';
        /** @var array<string,mixed> */
        private array $data = [];
        
        function __construct (string $token = '')
        {
            if ($token != '')
            {
                $this->data = (array) JWT::decode ($token, new Key (getenv ('JWT_HASH'), 'HS256'));
            }
        }

        /**
         * Searches for the given key in the session storage and returns it's value
         *
         * @param string $key The key to get
         *
         * @return mixed The value stored in that key
         */
        public function get (string $key): mixed
        {
            return $this->data [$key] ?? null;
        }

        public function set (string $key, $value): SessionStorage
        {
            $this->data [$key] = $value;

            return $this;
        }
        
        public function getToken (): string
        {
            return JWT::encode ($this->data, getenv ('JWT_HASH'), 'HS256');
        }
    };
